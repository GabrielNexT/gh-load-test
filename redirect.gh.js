import http from "k6/http";
import { check, sleep } from "k6";
import { SharedArray } from "k6/data";

export let options = {
  vus: 1,
  duration: "2h",
  iterations: 1,
};

const candidates = new SharedArray("candidates", () => {
  const candidates = JSON.parse(open("./candidates.json"));
  return candidates;
});

const stages = [
  "complete_stage_with_nexa",
  "default_stage",
  "complete_stage",
  "ocp_stage",
  "nexa_stage",
];

const id = parseInt(__ENV.ID);
const total = parseInt(__ENV.TOTAL);
const maxRetries = parseInt(__ENV.MAX_RETRIES) || 0;

const amountPerInstance = candidates.length / total || candidates.length;
const from = Math.floor(amountPerInstance * id || 0);
const to = Math.floor(from + amountPerInstance);

export default function () {
  const requestPerMachine = __ENV.REQUEST_PER_MACHINE || candidates.length;
  let count = 0;
  let i = from;
  let retries = 0;
  while (count != requestPerMachine) {
    if (count == requestPerMachine) break;
    const candidate = candidates[i];
    const randomStage = stages[Math.floor(Math.random() * stages.length)];
    const candidate_name = `${candidate.first_name} ${candidate.last_name}`;

    // Dev Env
    // const url = `https://angry-hound-37.loca.lt/api/v1/integrations/gh/register/v2/1/${candidate.application_id}?candidate_id=${candidate.candidate_id}&candidate_name=${candidate_name}&candidate_email=${candidate.email}&stage=${randomStage}`;
    // Sandbox Env
    const url = `https://sandbox-app.mindsight.com.br/api/v1/integrations/gh/register/11/${candidate.application_id}?candidate_id=${candidate.candidate_id}&candidate_name=${candidate_name}&candidate_email=${candidate.email}&stage=${randomStage}`;
    // Production Env
    // const url = `https://app.mindsight.com.br/api/v1/integrations/gh/register/36/${candidate.application_id}?candidate_id=${candidate.candidate_id}&candidate_name=${candidate_name}&candidate_email=${candidate.email}&stage=${randomStage}`;

    const endpoint = encodeURI(url);

    const params = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const res = http.get(endpoint, params);

    check(res, {
      "Respondeu com sucesso!": (r) => [200, 301].includes(r.status),
    });

    if (
      [200, 301].includes(res.status) ||
      !maxRetries ||
      retries >= maxRetries
    ) {
      count++;
      i++;
      retries = 0;
    } else {
      retries++;
    }

    if (i >= to) i = from;
    sleep(__ENV.SLEEP || 0);
  }
}
